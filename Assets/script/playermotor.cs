﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playermotor : MonoBehaviour
{
    private const float lanedistance = 3.0f;
    private const float directionspeed = 0.05f;
    private CharacterController charcontroller;
    private float jumpforce =8f;

    private float grafitiy = 12f;

    private float verticalvelecity;

    private float speed = 14;
    private int desiretlane = 1;//0 sol 1 orta 2 sağ
    Animator anim;

    private void Start()
    {
        charcontroller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();

    }
    private void Update()
    {
        if (mobilinput.Instance.Swipeleft)//bilgisayatiçin=Input.GetKeyDown(KeyCode.LeftArrow)
        {
            movelane(false);
        }
        if (mobilinput.Instance.Swiperight)
        {
            movelane(true);
        }
        Vector3 targetposition = transform.position.z * Vector3.forward;// zıpladıgımızdada ileri hareket edebiliriz
        if (desiretlane == 0)
        {
            targetposition += Vector3.left * lanedistance;
        }
        else if (desiretlane == 2)
        {
            targetposition += Vector3.right * lanedistance;
        }
        Vector3 movevector = Vector3.zero;// kayma
        movevector.x = (targetposition - transform.position).normalized.x * speed;// normalize sayesinde sayı negatif mi pozitif mi anlıyoruz

        if (charcontroller.isGrounded)// zıplama
        {
            verticalvelecity = -0.1f;
            if (mobilinput.Instance.Swipeup)// bilgisayar için Input.GetKeyDown(KeyCode.Space)
            {
                anim.Play("jump");//hata olma sebebi animatörün kapakterde olmaması çoçukta olması
                verticalvelecity = jumpforce;
            }

        }
        else// düşme
        {

            verticalvelecity -= (grafitiy * Time.deltaTime);
            if (mobilinput.Instance.Swipedown)// bilgisayar için =Input.GetKeyDown(KeyCode.Space)
            {
                verticalvelecity = -jumpforce;
            }
        }

        movevector.y = verticalvelecity;
        movevector.z = speed;
        charcontroller.Move(movevector * Time.deltaTime);
        Vector3 direction = charcontroller.velocity;//koşarken bedenin saga sola dönmesini sağlıyoruz
        if (direction != Vector3.zero)
        {
            direction.y = 0;
            transform.forward = Vector3.Lerp(transform.forward, direction, directionspeed);
        }
    }
    private void movelane(bool goingright)
    {
        desiretlane += (goingright) ? 1 : -1;// ne anlama geliyor bak
        desiretlane = Mathf.Clamp(desiretlane, 0, 2);// platfomdan dışarı çıkmamamızı sağlıyor
        //if (!goingright)// sağ sola hareket etmemizi saglıyor
        //{
        //    desiretlane--;
        //    if (desiretlane <= -1)// platformdan dışarı çıkmamızı sağlıyor
        //    {
        //        desiretlane = 0;
        //    }
        //}
        //if (goingright)
        //{
        //    desiretlane++;
        //    if (desiretlane >= 3)
        //    {
        //        desiretlane = 2;
        //    }
        //}
    }
    //private bool isgrounded()// burayı ögren
    //{
        
    //    Ray groundRay = new Ray (
    //         new Vector3
    //        (charcontroller.bounds.center.x,
    //        (charcontroller.bounds.center.y - charcontroller.bounds.extents.y + 0.2f),
    //        charcontroller.bounds.center.z),
    //        Vector3.down);
    //    Debug.DrawRay(groundRay.origin, groundRay.direction, Color.cyan, 1.0f);
    //    return Physics.Raycast(groundRay,0.2f+0.1f);
    //}
}
