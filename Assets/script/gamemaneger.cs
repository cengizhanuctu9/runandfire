using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gamemaneger : MonoBehaviour
{
    bool gamestart = false;
    public GameObject panel;
    private void Start()
    {
        Time.timeScale = 0;
    }
    public void Gamestart()
    {
        if (gamestart)
        {
            Time.timeScale = 0;
            gamestart = true;
        }
        else
        {

            Time.timeScale = 1;
            gamestart = false;
            panel.transform.GetChild(1).gameObject.SetActive(false);
        }
    }
    public void gameoverpanel()
    {
        Time.timeScale = 0;
        panel.transform.GetChild(2).gameObject.SetActive(true);
    }
    public void restartbutton()
    {
        SceneManager.LoadScene("SampleScene");
    }
}
