﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mobilinput : MonoBehaviour
{
    public static mobilinput Instance { set; get; }// singelton oluşturduk


    private const float deadzone = 100.0f;
    private bool top, swipeleft, swiperight, swipeup, swipedown;
    private Vector2 swipedelta, starttouch;

    public bool Top { get { return top; } }
    public Vector2 Swipedelta { get { return swipedelta; } }
    public bool Swipeleft { get { return swipeleft; } }
    public bool Swiperight { get { return swiperight; } }
    public bool Swipeup { get { return swipeup; } }
    public bool Swipedown { get { return swipedown; } }
    private void Awake()
    {
        Instance = this;
    }
    public void Update()
    {
        top = swipeleft = swiperight = swipeup = swipedown = false;

        #region bilgisayar kontrolleri
        if (Input.GetMouseButtonDown(0))
        {
            top = true;
            starttouch = Input.mousePosition;

        }
        else if (Input.GetMouseButtonUp(0))
        {
            starttouch = swipedelta = Vector2.zero;
        }
        #endregion
        #region telefon kontrolleri
        if (Input.touches.Length != 0)
        {
            if (Input.touches[0].phase==TouchPhase.Began)
            {
                top = true;
                starttouch = Input.mousePosition;

            }
            else if (Input.touches[0].phase == TouchPhase.Ended|| Input.touches[0].phase == TouchPhase.Canceled)
            {
                starttouch = swipedelta = Vector2.zero;
            }
        }

        #endregion

        swipedelta = Vector2.zero;
        if (starttouch != Vector2.zero)//burada dokundugumuz mesafeler arasını ölçtük
        {
            // telefon için 
            if (Input.touches.Length != 0)
            {
                swipedelta = Input.touches[0].position - starttouch;// başlangıç değerimizden dokundugumuz yerin kordinatlarını çıkartıyoruz [0] ilk dokundugun yer birinci parmak demek 
            }
            //bilgisayar için 
            if (Input.GetMouseButton(0))
            {
                swipedelta = (Vector2)Input.mousePosition - starttouch;// mouseposition vektör 3 bir degerdir onu çevirdik
            }

        }

        if (swipedelta.magnitude > deadzone)// vektör elemanlarının karelerini alıp toplayıp karekökünü alıyor
        {
            float x = swipedelta.x;
            float y = swipedelta.y;
            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                if (x < 0)
                {
                    swipeleft = true;
                }
                else
                    swiperight = true;
            }
            else
            {
                if (y < 0)
                {
                    swipedown = true;
                }
                else
                    swipeup = true;
            }
            starttouch = swipedelta = Vector2.zero;
        }
    }





}
