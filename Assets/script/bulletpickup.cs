using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletpickup : MonoBehaviour
{
    public int amount=10;
    Gunscript gunsc;
    private void Start()
    {
       gunsc = GameObject.FindObjectOfType<Gunscript>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gunsc.addbullet(amount);
            Destroy(gameObject);
            
        }
    }
}
