using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class silahim : MonoBehaviour
{
    public GameObject gunbarrel;
    public float range = 500;
    public bool permissiontoshoot = true;
    public float shootspeedout;
    public int bullets;
    public int replacement = 50;
    public int chargercapaside = 30;
    public float reloadspenttime;
    public AudioSource gunaudio;
    public ParticleSystem muzzle;
    public ParticleSystem bulletscar;
    public ParticleSystem bloadeffeck;
    float shootspeedin;

    IEnumerator reloadtime()
    {
        reload();
        yield return new WaitForSeconds(reloadspenttime);


    }
    private void Start()
    {
        bullets = chargercapaside;
    }
    private void FixedUpdate()
    {
        if (permissiontoshoot && Time.time > shootspeedin)
        {
            Shoot();
            shootspeedin = Time.time + shootspeedout;
        }

    }
    void Shoot()
    {
        RaycastHit hit;
        if (Physics.Raycast(gunbarrel.transform.position, gunbarrel.transform.forward, out hit, range) && bullets != 0)
        {

            gunaudio.Play();
            muzzle.Play();
            if (hit.transform.CompareTag("enemy"))
            {
                Instantiate(bloadeffeck, hit.point, Quaternion.LookRotation(hit.normal));
            }
            Instantiate(bulletscar, hit.point, Quaternion.LookRotation(hit.normal));
            bullets--;


        }
        else if (bullets == 0)
        {
            StartCoroutine(reloadtime());

        }

    }
    void reload()
    {
        if (replacement <= chargercapaside)
        {
            bullets = replacement;
            replacement = 0;
        }
        else
        {
            replacement -= chargercapaside;
            bullets = chargercapaside;
        }

    }

}
