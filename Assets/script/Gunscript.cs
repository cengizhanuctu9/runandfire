﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gunscript : MonoBehaviour
{
    public GameObject gunbarrel;
    public float range;
    public bool permissiontoshoot = true;
    public float shootspeedout;
    public int bullets;
    public int replacement = 50;
    public int chargercapaside = 30;
    public float reloadspenttime;
    public AudioSource gunaudio;
    public ParticleSystem muzzle;
    public ParticleSystem bulletscar;
    public ParticleSystem bloadeffeck;
    public float gundamage;
    float shootspeedin;
    public Text ingun;
    public Text inbag;

    private void Start()
    {
        bullets = chargercapaside;
        inbag.text = replacement.ToString();
        ingun.text = bullets.ToString();
    }
    private void FixedUpdate()
    {
        if (permissiontoshoot && Time.time > shootspeedin)
        {
            Shoot();
            shootspeedin = Time.time + shootspeedout;
        }

    }
    void Shoot()
    {

        int layerMask = 1 << 8;//düşman üretmek için kullandığım boş abjeler silah işini ile sorun yaşadığından sadece bu layer deki objeleri dikkae al dedim 8.layerden sonrasını dikkate al demek
        RaycastHit hit;
        if (Physics.Raycast(gunbarrel.transform.position, gunbarrel.transform.forward, out hit, range, layerMask) && bullets != 0)
        {


            if (hit.transform.CompareTag("enemy"))
            {
                hit.transform.gameObject.GetComponent<enemystatus>().getdamage(gundamage);
                Instantiate(bloadeffeck, hit.point, Quaternion.LookRotation(hit.normal));
                bullets--;
                inbag.text = replacement.ToString();
                ingun.text = bullets.ToString();
                gunaudio.Play();
                muzzle.Play();
            }
            // Instantiate(bulletscar, hit.point, Quaternion.LookRotation(hit.normal));



        }
        else if (bullets == 0)
        {
            reload();
           

        }

    }
    void reload()
    {
        if (replacement <= chargercapaside)
        {
            bullets = replacement;
            replacement = 0;
            inbag.text = replacement.ToString();
            ingun.text = bullets.ToString();
        }
        else
        {
            replacement -= chargercapaside;
            bullets = chargercapaside;
            inbag.text = replacement.ToString();
            ingun.text = bullets.ToString();
        }

    }
    public void addbullet(int bullet)
    {
        replacement += bullet;
    }

}
