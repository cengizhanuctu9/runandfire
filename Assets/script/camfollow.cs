﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camfollow : MonoBehaviour
{
    GameObject player;
    Vector3 mesafe;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        mesafe = transform.position - player.transform.position;
    }


    private void LateUpdate()
    {
        transform.position = player.transform.position + mesafe;
    }
}
